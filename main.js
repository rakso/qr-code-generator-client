document.addEventListener("DOMContentLoaded", () => {
  const form = document.querySelector("form");
  const image = document.getElementById("generated");

  form.addEventListener("change", () => {
    const formData = new URLSearchParams(new FormData(form)).toString();
    const generatedFormUrl = `${form.action}?${formData}`;

    image.src = generatedFormUrl;
  });
});
