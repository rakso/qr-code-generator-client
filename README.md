# QR Code generator client

Simple web page with a bunch of HTML used to query the data from https://www.qrcode-monkey.com

## There is "automated script" to generate QR with specific settings and logo

1. Make sure that your web server user has write permission to the project directory.
1. Paste your desired logo file into the root directory of project.
1. Copy the `variables.template.php` with name `variables.php`
1. Modify the `variables.php`
1. Navigate to `api.php?func=upload` to upload the image to the https://www.qrcode-monkey.com server.
1. Generate QR Code `api.php?func=qr&data=HelloWorld&fileName=this-is-my-qr-code`.

_There will be a logo in your QR for a while. If the logo does not appear, you can try uploading it again._
