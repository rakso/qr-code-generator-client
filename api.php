<?php
require 'variables.php';

$func = isset($_GET['func']) ? $_GET['func'] : '';

switch ($func) {
    case 'upload':
        uploadLogo($FILE_TO_UPLOAD_PATH, $UPLOADED_FILE_NAME_FILE_PATH);
        break;

    case 'qr':
        $data = isset($_GET['data']) ? $_GET['data'] : '';
        $fileName = isset($_GET['fileName']) ? $_GET['fileName'] : 'my-qr-code';
        $download = isset($_GET['download']) ? true : false;
        $fileType = isset($_GET['fileType']) ? $_GET['fileType'] : 'png';
        sendRequestForQrCode($data, $fileName, $download, $UPLOADED_FILE_NAME_FILE_PATH, $fileType);
        break;

    default:
        echo 'Bad request';
        break;
}

function sendRequestForQrCode($data, $fileName, $download = false, $fileWithFileNamePath = '', $fileType = 'png')
{

    $config = [
        "body" => "circular",
        "eye" => "frame1",
        "eyeBall" => "ball0",
        "erf1" => [
            "fh"
        ],
        "erf2" => [],
        "erf3" => [
            "fh",
            "fv"
        ],
        "brf1" => [],
        "brf2" => [],
        "brf3" => [],
        "bodyColor" => "#000000",
        "bgColor" => "#FFFFFF",
        "eye1Color" => "#000000",
        "eye2Color" => "#000000",
        "eye3Color" => "#000000",
        "eyeBall1Color" => "#000000",
        "eyeBall2Color" => "#000000",
        "eyeBall3Color" => "#000000",
        "gradientColor1" => "",
        "gradientColor2" => "",
        "gradientType" => "linear",
        "gradientOnEyes" => "true",
        "logo" => "",
        "logoMode" => "clean"
    ];

    if (file_exists($fileWithFileNamePath)) {
        $fileNameFromFile = file_get_contents($fileWithFileNamePath);
        $config['logo'] = $fileNameFromFile;
    }

    $getParams = [
        "data" => $data,
        "download" => false,
        "file" => $fileType,
        "size" => 300,
        "config" => json_encode($config),
    ];

    $target_url = 'https://api.qrcode-monkey.com/qr/custom';

    $asAttachment = '';
    if ($download) {
        $asAttachment = ' attachment;';
    }

    $contentMimeType = "image/$fileType";
    switch ($fileType) {
        case 'svg':
            $contentMimeType = 'image/svg+xml';
            break;
        case 'pdf':
            $contentMimeType = 'application/pdf';
            break;
    }

    header("Content-Type: $contentMimeType");
    header('Content-Disposition:' . $asAttachment . ' filename="' . rawurlencode("$fileName.$fileType") . '"');
    echo sendRequest($target_url . '?' . http_build_query($getParams));
}

function sendRequest($url, $parseJson = false, $postFields = null)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    if (!empty($postFields)) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    }
    $result = curl_exec($ch);
    curl_close($ch);

    if ($parseJson) {
        return json_decode($result);
    }
    return $result;
}

function uploadFile($url, $filePath)
{
    if (function_exists('curl_file_create')) { // php 5.5+
        $cFile = curl_file_create($filePath);
    } else {
        $cFile = '@' . realpath($filePath);
    }

    $postFields = ['file' => $cFile];
    return sendRequest($url, true, $postFields);
}

function uploadLogo($filePathToUpload, $fileWithFileNamePath)
{
    $targetUrl = "https://api.qrcode-monkey.com/qr/uploadimage";

    $resultObject = uploadFile($targetUrl, $filePathToUpload);
    $uploadedFileName = $resultObject->file;

    $myfile = fopen($fileWithFileNamePath, "w") or die("Unable to open file to save the result!");
    fwrite($myfile, $uploadedFileName);
    fclose($myfile);

    echo 'Icon has been uploaded and is ready to use. Thank you very much.';
}
